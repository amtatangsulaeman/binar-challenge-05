//
//  DummyApi.swift
//  Challenge-05
//
//  Created by Tatang Sulaeman on 22/04/22.
//

import UIKit

func getUser() {
    guard let url = URL(string: "https://dummyapi.io/data/v1/user") else { return }

    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.setValue("6262b3304ac4a549b1c11428", forHTTPHeaderField: "app-id")

    URLSession.shared.dataTask(with: request) { (data, response, error) in
        print(response)
        print(data)
    }.resume()
}
